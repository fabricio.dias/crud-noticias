const PORT = process.env.PORT || 3003;
const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const connectDB = require('./config/db');
const app = express();
const fileUpload = require('express-fileupload');



connectDB();

//init middleware
app.use(cors());
app.use(express.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use('/uploads', express.static('uploads'));
app.use(fileUpload({ createParentPath: true }));

//init routes

//Main Page
app.get('/', (req, res) => {
    res.send("Main Page");
});

//Route user
app.use('/user', require('./routes/user'));

//Router News
app.use('/news', require('./routes/news'));

//Route Auth
app.use('/login', require('./routes/auth'));

//init Server
app.listen(PORT, () => console.log("Server rodando"));