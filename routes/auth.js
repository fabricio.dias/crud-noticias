const express = require('express');
const router = express.Router();
const { body, validationResult } = require('express-validator');
const bcrypt = require('bcryptjs');
const User = require('../models/user');
const jwt = require('jsonwebtoken');
const config = require('config');

router.post('/', [
    body("email").isEmail().withMessage("email tem que estar no formato xxx@abc.yyy"),
    body("password").exists().withMessage("password é obrigatória")
], async (req, res) => {
    try {
        const erros = validationResult(req)
        if (!erros.isEmpty()) {
            return res.status(400).json({ erros: erros.array() })
        }
        const { email, password } = req.body
        //procurar usuario com esse email
        const user = await User.findOne({ email }).select('id email name password userType')
        if (!user) {
            return res.status(404).json({ erros: [{ param: 'email', msg: 'Usuario nao existe' }] })
        } else {
            const senhaCorreta = await bcrypt.compare(password, user.password)
            if (!senhaCorreta) {
                return res.status(400).json({ erros: [{ param: 'senha', msg: 'Senha esta incorreta' }] })
            }
            else {
                const payload = {
                    user: {
                        id: user.id,
                        name: user.name,
                        email: user.email,
                        userType: user.userType,
                    }
                }
                jwt.sign(
                    payload,
                    config.get('jwtSecret'),
                    { expiresIn: '5 days' },
                    (err, token) => {
                        if (err) throw err;
                        payload.token = token
                        return res.status(200).json(payload)
                    }
                )
            }
        }

    } catch (error) {
        console.log(error.message)
        res.status(500).send({ "Erro:": "Server error" })
    }
});

module.exports = router;