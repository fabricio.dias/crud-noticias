const express = require('express');
const router = express.Router();
const { body, validationResult } = require('express-validator');
const auth = require('../middleware/auth');
const file = require('../middleware/file');
const New = require('../models/news');
const User = require('../models/user');

//init routes

//Post News

router.post('/', auth, file, [
    body('title').not().isEmpty().withMessage('Título tem que ser preenchido'),
    body('content').not().isEmpty().withMessage('Conteúdo tem que ser preenchido'),
    body('category').not().isEmpty().withMessage('Categoria tem que ser preenchido'),
], async (req, res) => {
    try {
        const createdBy = req.user.id
        let { title, content, category, photo } = req.body

        const erros = validationResult(req)
        if (!erros.isEmpty()) {
            return res.status(400).json({ erros: erros.array() })
        } else {
            let noticia = new New({ title, content, category, photo, createdBy })
            await noticia.save()
            if (noticia.id) {
                return res.status(200).json(noticia)
            }
        }
    } catch (err) {
        console.error(err.message)
        res.status(500).send({ "error": 'Erro na realizaçao da requisiçao' })
    }
});

//Get New By ID
router.get('/:id', async (req, res, next) => {
    try {
        let id = req.params.id
        const noticia = await New.findById({ _id: id }).lean(true)
        if (noticia) {
            let newObjNew = {
                ...noticia
            }
            const user = await User.findById(noticia.createdBy).lean(true) //Transforma a query do mongoose em array JS
            if (user) {
                newObjNew.createdBy = {
                    user_id: user._id,
                    user_name: user.name
                }
            }
            else {
                newObjNew.createdBy = {
                    user_id: "xxxxxx",
                    user_name: "N/A"
                }
            }
            return res.status(200).json(newObjNew);
        } else {
            return res.status(404).json({ erros: 'Usuario nao encontrado' })
        }
    } catch (err) {
        console.error(err.message)
        res.status(500).send({ "error": 'Erro na realizaçao da requisiçao' })
    }
});


//Get All News

router.get('/', async (req, res, next) => {
    try {
        let news = await New.find().lean(true)
        if (news.length > 0) {
            const newArrNews = news.map(async (value) => {
                let newObjNew = {
                    ...value
                }
                const user = await User.findById(value.createdBy).lean(true) //Transforma a query do mongoose em array JS
                if (user) {
                    newObjNew.createdBy = {
                        user_id: user._id,
                        user_name: user.name
                    }
                }
                else {
                    newObjNew.createdBy = {
                        user_id: "xxxxxx",
                        user_name: "N/A"
                    }
                }
                return newObjNew
            })
            Promise.all(newArrNews).then(function (results) {
                return res.status(200).json(results)
            })
        } else {
            return res.status(404).json({ erros: "Nao tem noticias" })
        }
    } catch (err) {
        console.error(err.message)
        res.status(500).send({ "error": 'Erro na realizaçao da requisiçao' })
    }
});

// get news de um reporter
router.get('/reporter/:id', async (req, res, next) => {
    try {
        let id = req.params.id
        console.log(id)
        let news = await New.find({ createdBy: id }).lean(true)
        if (news.length > 0) {
            const newArrNews = news.map(async (value) => {
                let newObjNew = {
                    ...value
                }
                const user = await User.findById(value.createdBy).lean(true) //Transforma a query do mongoose em array JS
                if (user) {
                    newObjNew.createdBy = {
                        user_id: user._id,
                        user_name: user.name
                    }
                }
                else {
                    newObjNew.createdBy = {
                        user_id: "xxxxxx",
                        user_name: "N/A"
                    }
                }
                return newObjNew
            })
            Promise.all(newArrNews).then(function (results) {
                return res.status(200).json(results)
            })
        } else {
            return res.status(404).json({ erros: "Nao tem noticias" })
        }
    } catch (err) {
        console.error(err.message)
        res.status(500).send({ "error": 'Erro na realizaçao da requisiçao' })
    }
});

//Patch News

router.patch('/:id', auth, file, async (req, res, next) => {
    try {

        let bodyRequest = req.body
        const createdBy = req.user.id
        bodyRequest.createdBy = createdBy
        const id = req.params.id
        const update = { $set: bodyRequest }
        const noticia = await New.findByIdAndUpdate(id, update, { new: true })
        if (noticia) {
            res.send(noticia)
        } else {
            res.status(404).send({ erros: 'Usuario nao encontrado' })
        }
    } catch (err) {
        console.error(err.message)
        res.status(500).send({ "error": 'Erro na realizaçao da requisiçao' })
    }
});

//Delete News

router.delete('/:id', async (req, res, next) => {
    try {
        let id = req.params.id
        const noticia = await New.findByIdAndDelete(id)
        if (noticia) {
            return res.status(200).json(noticia)
        } else {
            return res.status(404).json({ erros: 'Usuario nao encontrado' })
        }
    } catch (err) {
        console.error(err.message)
        res.status(500).send({ "error": 'Erro na realizaçao da requisiçao' })
    }
});

//Route validate news by user
router.patch('/validate/:id', async (req, res, next) => {
    try {
        let id = req.params.id
        const noticia = await New.findById({ _id: id })
        if (noticia) {
            let isActive = !noticia.is_active
            const update = { $set: { "is_active": isActive } }
            const updateNoticia = await New.findByIdAndUpdate(id, update, { new: true })
            return res.status(200).json(updateNoticia)
        } else {
            return res.status(404).json({ erros: 'Noticia nao encontrado' })
        }
    } catch (err) {
        console.error(err.message)
        res.status(500).send({ "error": 'Erro na realizaçao da requisiçao' })
    }
});

//Route highlight news by user
router.patch('/highlight/:id', async (req, res, next) => {
    try {
        let id = req.params.id
        const noticia = await New.findById({ _id: id })
        if (noticia) {
            let isHighlight = !noticia.highlight
            const update = { $set: { "highlight": isHighlight } }
            const updateNoticia = await New.findByIdAndUpdate(id, update, { new: true })
            return res.status(200).json(updateNoticia)
        } else {
            return res.status(404).json({ erros: 'Noticia nao encontrado' })
        }
    } catch (err) {
        console.error(err.message)
        res.status(500).send({ "error": 'Erro na realizaçao da requisiçao' })
    }
});

module.exports = router;