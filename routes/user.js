const express = require('express');
const router = express.Router();
const { body, validationResult } = require('express-validator');
const bcrypt = require('bcryptjs');
const User = require('../models/user');
const auth = require('../middleware/auth');
const New = require('../models/news');

//init routes

//Post Comom User
router.post('/', [
    body('name').not().isEmpty().withMessage('Nome tem que ser preenchido'),
    body('email').isEmail().withMessage('Email tem que ser num formato valido').bail().custom(value => {
        return User.findOne({ 'email': value }).then(user => {
            if (user) {
                return Promise.reject('E-mail está em uso');
            }
        });
    }),
    body('password').isLength({ min: 6 }).withMessage('Senha tem que ter 6 digitos')
], async (req, res) => {
    try {
        let { name, email, password } = req.body
        const erros = validationResult(req)
        if (!erros.isEmpty()) {
            return res.status(400).json({ erros: erros.array() })
        } else {
            const salt = await bcrypt.genSalt(10)
            let user = new User({ name, email, password })
            user.password = await bcrypt.hash(password, salt)
            await user.save()
            if (user.id) {
                return res.status(200).json(user)
            }
        }
    } catch (err) {
        console.error(err.message)
        res.status(500).send({ "error": 'Erro na realizaçao da requisiçao' })
    }
});

//POST REPORTER E ADMIN

router.post('/new-user', [auth,
    body('name').not().isEmpty().withMessage('Nome tem que ser preenchido'),
    body('email').isEmail().withMessage('Email tem que ser num formato valido').bail().custom(value => {
        return User.findOne({ 'email': value }).then(user => {
            if (user) {
                return Promise.reject('E-mail está em uso');
            }
        });
    }),
    body('password').isLength({ min: 6 }).withMessage('Senha tem que ter 6 digitos'),
    body('userType').not().isEmpty().withMessage('O tipo de usuário tem que ser preenchido'),

], async (req, res) => {
    try {
        let { name, email, password, userType } = req.body
        const erros = validationResult(req)
        if (!erros.isEmpty()) {
            return res.status(400).json({ erros: erros.array() })
        } else {
            const salt = await bcrypt.genSalt(10)
            let user = new User({ name, email, password, userType })
            user.password = await bcrypt.hash(password, salt)
            await user.save()
            if (user.id) {
                return res.status(200).json(user)
            }
        }
    } catch (err) {
        console.error(err.message)
        res.status(500).send({ "error": 'Erro na realizaçao da requisiçao' })
    }
});

//Get User By ID
router.get('/:id', auth, async (req, res, next) => {
    try {
        let id = req.params.id
        const user = await User.findById({ _id: id }).lean(true)
        if (user) {
            if(user.userType == "reporter"){
                let newObjReporter = {
                    ...user
                }
                const news = await New.find({ "createdBy": user._id }).lean(true) //Transforma a query do mongoose em array JS
                if (news.length > 0) {
                    newObjReporter.news = news
                }
                else {
                    newObjReporter.news = []
                }
                return res.status(200).json(newObjReporter)
            }
            return res.status(200).json(user)
        } else {
            return res.status(404).json({ erros: 'Usuario nao encontrado' })
        }
    } catch (err) {
        console.error(err.message)
        res.status(500).send({ "error": 'Erro na realizaçao da requisiçao' })
    }
});

//Get All Users

router.get('/', async (req, res, next) => {
    try {
        let users = await User.find()
        if (users.length > 0) {
            return res.status(200).json(users)
        } else {
            return res.status(404).json({ erros: "Nao tem usuarios" })
        }
    } catch (err) {
        console.error(err.message)
        res.status(500).send({ "error": 'Erro na realizaçao da requisiçao' })
    }
});

//Patch User
router.patch('/:id', auth, async (req, res, next) => {
    try {

        let bodyRequest = req.body
        const id = req.params.id
        const salt = await bcrypt.genSalt(10)
        if (bodyRequest.password) {
            bodyRequest.password = await bcrypt.hash(bodyRequest.password, salt)
        }
        const update = { $set: bodyRequest }
        const user = await User.findByIdAndUpdate(id, update, { new: true })
        if (user) {
            res.send(user)
        } else {
            res.status(404).send({ erros: 'Usuario nao encontrado' })
        }
    } catch (err) {
        console.error(err.message)
        res.status(500).send({ "error": 'Erro na realizaçao da requisiçao' })
    }
});

//Delete User

router.delete('/:id', auth, async (req, res, next) => {
    try {
        let id = req.params.id
        const user = await User.findByIdAndDelete(id)
        if (user) {
            return res.status(200).json(user)
        } else {
            return res.status(404).json({ erros: 'Usuario nao encontrado' })
        }
    } catch (err) {
        console.error(err.message)
        res.status(500).send({ "error": 'Erro na realizaçao da requisiçao' })
    }
});


//Get All Reporters

router.get('/all/reporters',auth ,async (req, res, next) => {
    try {
        let users = await User.find({ "userType": "reporter" }).lean(true)
        if (users.length > 0) {
            const newArrReporters = users.map(async (value) => {
                let newObjReporter = {
                    ...value
                }
                const news = await New.find({ "createdBy": value._id }).lean(true) //Transforma a query do mongoose em array JS
                if (news.length > 0) {
                    newObjReporter.news = news
                }
                else {
                    newObjReporter.news = []
                }
                return newObjReporter
            })
            Promise.all(newArrReporters).then(function (results) {
                return res.status(200).json(results)
            })
        } else {
            return res.status(404).json({ erros: "Nao tem reporters" })
        }
    } catch (err) {
        console.error(err.message)
        res.status(500).send({ "error": 'Erro na realizaçao da requisiçao' })
    }
});



module.exports = router;