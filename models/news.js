const mongoose = require('mongoose');

const NewsSchema = mongoose.Schema({
    title: {
        type: String,
        required: true
    },
    content: {
        type: String,
        required: true
    },
    createdBy: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'user',
        required: true
    },
    createdAt: {
        type: Date,
        default: Date.now
    },
    photo: {
        type: String,
        required: true
    },
    category: {
        type: String,
        required: true
    },
    highlight: {
        type: Boolean,
        default: false
    },
    is_active: {
        type: Boolean,
        default: false
    }
}, { autoCreate: true })

module.exports = mongoose.model('new', NewsSchema);