const mongoose = require('mongoose');

const UserSchema = mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    userType: {
        type: String,
        enum: ['reporter', 'comum', 'administrador'],
        default: 'comum'
    },
    createdAt: {
        type: Date,
        default: Date.now
    }
}, { autoCreate: true })

module.exports = mongoose.model('user', UserSchema);