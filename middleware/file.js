const slugfy = require('../service/slugfy');
const AWS = require('aws-sdk');
const config = require('config');
const fs = require('fs');


module.exports = async function (req, res, next) {
    try {
        const BUCKET_NAME = process.env.AWS_S3_BUCKETNAME || config.get('AWS_S3_BUCKETNAME')

        const s3 = new AWS.S3({
            accessKeyId: process.env.AWS_S3_ID || config.get('AWS_S3_ID'),
            secretAccessKey: process.env.AWS_S3_SECRET || config.get('AWS_S3_SECRET')
        });

        if (!req.files) {
            if (req.method == 'PATCH') {
                next()
            } else {
                res.status(400).send({ error: "Arquivo Não Enviado" });
            } 
        } else {
            let photo = req.files.photo
            const name = slugfy(photo.name)
            req.body.photo_name = name

            if (photo.mimetype.includes('image/')) {
                const file = await photo.mv(`./uploads/${name}`)
                const params = {
                    Bucket: BUCKET_NAME,
                    ACL: 'public-read',
                    Key: `images/${name}`, // File name you want to save as in S3
                    Body: fs.createReadStream(`./uploads/${name}`)
                };
                s3.upload(params, function (err, data) {
                    if (err) {
                        console.error(err);
                        res.status(500).send(err);
                    } else {
                        req.body.photo = data.Location;
                        console.log(`File uploaded successfully. ${data.Location}`);
                        fs.unlinkSync(`./uploads/${name}`)
                        next()
                    }
                })

            } else {
                res.status(400).send({ error: "Formato Inválido" });
            }

        }

    } catch (err) {
        res.status(500).send({ "error": err.message })
    }
}