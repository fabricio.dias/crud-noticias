const jwt = require('jsonwebtoken');
const config = require('config');
const ADMIN_ROUTES = ['/user/new-user/'];
const REPORTER_ROUTES = ['/news/'];

module.exports = function (req, res, next) {
    const token = req.header('x-auth-token');
    if (!token) {
        return res.status(401).json({ msg: 'No token, authorization denied' });
    }
    try {
        jwt.verify(token, config.get('jwtSecret'), (error, decoded) => {
            if (error) {
                console.log('toke nao valido')
                return res.status(401).json({ msg: 'Token is not valid' });
            } console.log(req.originalUrl)
            if (ADMIN_ROUTES.includes(req.originalUrl) && decoded.user.userType != "administrador") {
                return res.status(401).json({ msg: 'usuário não é admin' });
            }
            if (REPORTER_ROUTES.includes(req.originalUrl) && decoded.user.userType != "reporter") {
                return res.status(401).json({ msg: 'usuário não é reporter' });
            }
            req.user = decoded.user;
            next()
        })
    } catch (err) {
        console.error('something wrong with auth middleware');
        return res.status(500).json({ msg: 'Server Error' });
    }
}